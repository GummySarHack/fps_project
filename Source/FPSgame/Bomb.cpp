// Fill out your copyright notice in the Description page of Project Settings.


#include "Bomb.h"


ABomb::ABomb()
{
    if (!ProjectileMovementComponent)
    {
        // Use this component to drive this projectile's movement.
        ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
       // ProjectileMovementComponent->SetUpdatedComponent(CollisionComponent);
        
        ProjectileMovementComponent->MaxSpeed = 3000.0f;
        ProjectileMovementComponent->bRotationFollowsVelocity = true;
        ProjectileMovementComponent->bShouldBounce = true;
        ProjectileMovementComponent->Bounciness = 0.3f;
        ProjectileMovementComponent->ProjectileGravityScale = 0.0f;
    }
}

// Function that initializes the projectile's velocity in the shoot direction.
void ABomb::FireInDirection(const FVector& ShootDirection)
{
    ProjectileMovementComponent->Velocity = ShootDirection * ProjectileMovementComponent->InitialSpeed;
}

//
void ABomb::BindBombToCharacter(AFPSgameCharacter* TargetCharacter)
{
    Character = TargetCharacter;
    if (IsValid(Character))
    {
        UWeaponSystem* weaponSystem = Cast<UWeaponSystem>(Character->GetComponentByClass(UWeaponSystem::StaticClass()));
        FWeaponData weapondata = weaponSystem->GetWeaponData();

        weapondata.nbCurrentBombs++;

        weaponSystem->Bombs.AddUnique(this);
        

        //HandOffset = Character->GetMesh1P()->GetRelativeLocation();
        HandOffset = FVector(100.0f, 0.0f, 10.0f);

        if (weaponSystem->Bombs.Num() == 1)
        {
            Character->OnThrowObject.AddDynamic(this, &ABomb::ThrowBomb);
        }

        FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, true);
        this->AttachToComponent(Character->GetMesh1P(), AttachmentRules, FName(TEXT("GripPoint")));
        this->IsHeld = true;

        this->SetActorHiddenInGame(true);
    }
}

//What happens when the bomb is thrown
void ABomb::ThrowBomb()
{
    UWeaponSystem* weaponSystem = Cast<UWeaponSystem>(Character->GetComponentByClass(UWeaponSystem::StaticClass()));
    // Try and fire a projectile
    if (BombProjectileClass != nullptr)
    {
        
        weaponSystem->Bombs.RemoveAt(weaponSystem->Bombs.Num()-1);

        UWorld* const World = GetWorld();
        if (World != nullptr)
        {
            weaponSystem->WeaponData.nbCurrentBombs--;

            APlayerController* PlayerController = Cast<APlayerController>(Character->GetController());
            
            const FRotator SpawnRotation = PlayerController->PlayerCameraManager->GetCameraRotation();
            const FVector SpawnLocation = Character->GetActorLocation() + SpawnRotation.RotateVector(HandOffset);


            //Set Spawn Collision Handling Override
            FActorSpawnParameters ActorSpawnParams;
            ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

            // Spawn the projectile at the muzzle
            World->SpawnActor<ABombProjectile>(BombProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
        }
    }

    if (weaponSystem->Bombs.IsEmpty())
    {
        this->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
        Character->OnThrowObject.RemoveDynamic(this, &ABomb::ThrowBomb);
        Destroy();
    }


    if (GEngine)
        GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Throw Bomd!"));
}



void ABomb::HideBomb()
{
    //this->SetActorHiddenInGame((!this->GetHidden));
}
