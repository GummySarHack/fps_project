// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon.h"
#include "FPSgameCharacter.h"
#include "BombProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"

#include "Bomb.generated.h"

/**
 * 
 */
UCLASS()
class FPSGAME_API ABomb : public AWeapon
{
	GENERATED_BODY()
	
public:

    ABomb();

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
    FVector HandOffset;

	// Projectile movement component.
    UPROPERTY(VisibleAnywhere, Category = Movement)
    UProjectileMovementComponent* ProjectileMovementComponent;

    AFPSgameCharacter* Character;

    /** Projectile class to spawn */
    UPROPERTY(EditDefaultsOnly, Category = Projectile)
    TSubclassOf<class ABombProjectile> BombProjectileClass;



    // Function that initializes the projectile's velocity in the shoot direction.
    UFUNCTION()
    void FireInDirection(const FVector& ShootDirection);

    UFUNCTION(BlueprintCallable)
    void BindBombToCharacter(AFPSgameCharacter* TargetCharacter);

    UFUNCTION()
    void ThrowBomb();

    UFUNCTION()
    void HideBomb();

};
