// Fill out your copyright notice in the Description page of Project Settings.


#include "AmmoIncrease.h"
#include "WeaponSystem.h"


// Sets default values
AAmmoIncrease::AAmmoIncrease()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SphereComp = CreateDefaultSubobject<USphereComponent>(FName("SphereComp"));
	SphereComp->SetSphereRadius(50.f);
	SphereComp->OnComponentBeginOverlap.AddDynamic(this, &AAmmoIncrease::OnSphereBeginOverlap);
}

// Called when the game starts or when spawned
void AAmmoIncrease::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAmmoIncrease::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AAmmoIncrease::OnSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Character = Cast<AFPSgameCharacter>(OtherActor);

	if (Character != nullptr)
	{
		UWeaponSystem* weaponSystem = Cast<UWeaponSystem>(Character->GetComponentByClass(UWeaponSystem::StaticClass()));

		weaponSystem->WeaponData.nbAmmoUnload = weaponSystem->WeaponData.nbAmmoUnload + NbAmmoIncrease;

		//If our number of unloaded ammo is higher than our reserve allows, trim it down to max
		if (weaponSystem->WeaponData.nbAmmoUnload > weaponSystem->WeaponData.maxAmmoReserve)
		{
			weaponSystem->WeaponData.nbAmmoUnload = weaponSystem->WeaponData.maxAmmoReserve;
		}

		Destroy();
	}
}

