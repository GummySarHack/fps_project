#pragma once

#include "CoreMinimal.h"

#include "WeaponData.generated.h"

USTRUCT(Blueprintable)
struct FWeaponData
{
	GENERATED_BODY()

	/*
	*	Gun data
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
	float nbAmmoLoad = 0; //max per shoots before needing to reload
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
	float maxAmmoReserve = 0; //max in reserve
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
	float currentNbAmmo = 0; //how much was shoot
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
	float nbAmmoUnload = 0; //reserve left over
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo") // AB should add meta = (ClampMin = "0.1")
	float reloadTime = 0; // how long it takes to add ammoload to current

	/*
	*	Bomb date
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
	float nbMaxBombs = 0; //max nb bombs character can carry
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
	float nbCurrentBombs = 0; //nb bombs character being carried

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
	int Damage = 0;
};