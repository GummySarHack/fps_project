// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon.h"

// Sets default values
AWeapon::AWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	WeaponComponent = CreateDefaultSubobject<UTP_WeaponComponent>(FName("WeaponComponent"));
	PickUpComponent = CreateDefaultSubobject<UTP_PickUpComponent>(FName("PickUpComponent"));
	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(FName("USkeletalMesh"));

	SkeletalMesh->AttachToComponent(PickUpComponent, FAttachmentTransformRules::KeepRelativeTransform);
}
