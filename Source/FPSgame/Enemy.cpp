// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "Materials/MaterialInstanceDynamic.h"

PRAGMA_DISABLE_OPTIMIZATION


// Sets default values
AEnemy::AEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	NiagaraComponent = CreateDefaultSubobject<UNiagaraComponent>(TEXT("Niagara"));
	NiagaraComponent->SetupAttachment(GetCapsuleComponent());
	NiagaraComponent->DeactivateImmediate();
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}


void AEnemy::DecreaseHealth(int damageTaken)
{
	Health = Health - damageTaken;

	if (Health <= 0)
	{
		

		//UMaterialParameterCollectionInstance* MaterialParameterCollectionInstance = 
		//	GetWorld()->GetParameterCollectionInstance(MaterialParam);

		////auto param = MaterialParam.parameter

		//MaterialParameterCollectionInstance->SetScalarParameterValue(TEXT("Level"), -150.f);

		UCapsuleComponent* CapsuleComp = GetCapsuleComponent();
		CapsuleComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		CapsuleComp->SetCollisionResponseToAllChannels(ECR_Ignore);

		GetMesh()->SetCollisionProfileName(TEXT("Ragdoll"));
		GetMesh()->SetAllBodiesSimulatePhysics(true);
		GetMesh()->WakeAllRigidBodies();
		GetMesh()->bBlendPhysics = true;
		SetActorEnableCollision(true);


		UCharacterMovementComponent* CharacterComp = Cast<UCharacterMovementComponent>(GetMovementComponent());
		if (CharacterComp)
		{
			CharacterComp->StopMovementImmediately();
			CharacterComp->DisableMovement();
			CharacterComp->SetComponentTickEnabled(false);
		}

		GetWorldTimerManager().SetTimer(UnusedHandle, this,
										&AEnemy::StartNiagaraDeath, 2.f, false);
		
	}
}

void AEnemy::StartNiagaraDeath()
{
	NiagaraComponent->ActivateSystem();
	GetWorldTimerManager().SetTimer(UnusedHandle, this,
									&AEnemy::DestroyEnemyInstance, 3.f, false);
}

void AEnemy::DestroyEnemyInstance()
{
	Destroy();
}

PRAGMA_ENABLE_OPTIMIZATION