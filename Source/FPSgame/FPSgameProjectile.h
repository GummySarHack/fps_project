// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FPSgameProjectile.generated.h"

class USphereComponent;
class UProjectileMovementComponent;

UCLASS(config=Game)
class AFPSgameProjectile : public AActor
{
	GENERATED_BODY()


protected:
	/** Sphere collision component */
	UPROPERTY(VisibleDefaultsOnly, Category=Projectile)
	USphereComponent* CollisionComp;

	/** Projectile movement component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	UProjectileMovementComponent* ProjectileMovement;

public:
	AFPSgameProjectile();

	/** called when projectile hits something */
	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, 
					   AActor* OtherActor, 
					   UPrimitiveComponent* OtherComp, 
					   FVector NormalImpulse, 
					   const FHitResult& Hit);

	/** Returns CollisionComp subobject **/
	USphereComponent* GetCollisionComp() const { return CollisionComp; }
	/** Returns ProjectileMovement subobject **/
	UProjectileMovementComponent* GetProjectileMovement() const { return ProjectileMovement; }



	UPROPERTY(EditAnywhere)
	int ProjectileDamage = 0;



private:
	/** called when projectile hits something */
	
	virtual void DoOnHit(UPrimitiveComponent* HitComp,
						 AActor* OtherActor,
						 UPrimitiveComponent* OtherComp,
						 FVector NormalImpulse,
						 const FHitResult& Hit);
};

