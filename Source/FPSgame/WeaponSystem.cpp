// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponSystem.h"
#include "FPSgameCharacter.h"
#include "TP_WeaponComponent.h"

// Sets default values for this component's properties
UWeaponSystem::UWeaponSystem()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	
	Character = Cast<AFPSgameCharacter>(GetOwner());


	if (Character != nullptr)
	{
		Character->OnSwitchWeapon.AddDynamic(this, &UWeaponSystem::SwitchWeapon);
	}
}


void UWeaponSystem::SwitchWeapon()
{
	//Get other weapon
	auto weaponToHold = Weapons.FindByPredicate(
		[&](AWeapon* weapon) 
		{
			return !weapon->IsHeld;
		});

	//Remove current weapon
	auto weaponToHide = Weapons.FindByPredicate(
		[&](AWeapon* weapon)
		{
			return weapon->IsHeld;
		});
	
	if (weaponToHold != nullptr && weaponToHide != nullptr)
	{

		auto weaponCompToHide = Cast<UTP_WeaponComponent>((*weaponToHide)->GetComponentByClass(UTP_WeaponComponent::StaticClass()));
		weaponCompToHide->UnbindWeaponToCharacter();

		auto weaponCompToHold = Cast<UTP_WeaponComponent>((*weaponToHold)->GetComponentByClass(UTP_WeaponComponent::StaticClass()));
		weaponCompToHold->BindWeaponToCharacter();
	}

}

