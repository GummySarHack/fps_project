// Copyright Epic Games, Inc. All Rights Reserved.


#include "TP_WeaponComponent.h"
#include "FPSgameCharacter.h"
#include "FPSgameProjectile.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/Actor.h"
#include "Camera/PlayerCameraManager.h"

#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UTP_WeaponComponent::UTP_WeaponComponent()
{
	// Default offset from the character location for projectiles to spawn
	MuzzleOffset = FVector(100.0f, 0.0f, 10.0f);
}


void UTP_WeaponComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if(Character != nullptr)
	{
		// Unregister from the OnUseItem Event
		Character->OnUseItem.RemoveDynamic(this, &UTP_WeaponComponent::Fire);
	}
}

void UTP_WeaponComponent::Fire()
{

	UWeaponSystem* weaponSystem = Cast<UWeaponSystem>(Character->GetComponentByClass(UWeaponSystem::StaticClass()));

	if(Character == nullptr || Character->GetController() == nullptr)
	{
		return;
	}

	if (weaponSystem->WeaponData.currentNbAmmo > 0)
	{
		weaponSystem->WeaponData.currentNbAmmo--;
	}
	else if (weaponSystem->WeaponData.maxAmmoReserve == 0 || weaponSystem->WeaponData.nbAmmoUnload == 0)
	{
		return;
	}
	else if (weaponSystem->WeaponData.currentNbAmmo == 0)
	{
		if (!IsReloading)
		{
			IsReloading = true;
			GetOwner()->SetActorHiddenInGame(true);
			
			GetOwner()->GetWorldTimerManager().SetTimer(UnusedHandle, this, 
								&UTP_WeaponComponent::OnTimerReloadAmmo, WeaponData.reloadTime, false);
		}
		
		return;
	}

	// Try and fire a projectile
	if (ProjectileClass != nullptr)
	{
		UWorld* const World = GetWorld();
		if (World != nullptr)
		{
			APlayerController* PlayerController = Cast<APlayerController>(Character->GetController());
			const FRotator SpawnRotation = PlayerController->PlayerCameraManager->GetCameraRotation();
			// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
			const FVector SpawnLocation = GetOwner()->GetActorLocation() + SpawnRotation.RotateVector(MuzzleOffset);
	
			//Set Spawn Collision Handling Override
			FActorSpawnParameters ActorSpawnParams;
			ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
	
			// Spawn the projectile at the muzzle
			auto projectile = World->SpawnActor<AFPSgameProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);

			auto gun = Cast<AWeapon>(GetOwner());

			if (IsValid(gun))
			{
				projectile->ProjectileDamage = weaponSystem->WeaponData.Damage;
			}

		}
	}
	
	// Try and play the sound if specified
	if (FireSound != nullptr)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, Character->GetActorLocation());
	}
	
	// Try and play a firing animation if specified
	if (FireAnimation != nullptr)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = Character->GetMesh1P()->GetAnimInstance();
		if (AnimInstance != nullptr)
		{
			AnimInstance->Montage_Play(FireAnimation, 1.f);
		}
	}
}

void UTP_WeaponComponent::OnTimerReloadAmmo()
{
	IsReloading = false;
	//AmmoCalculation();

	UWeaponSystem* weaponSystem = Cast<UWeaponSystem>(Character->GetComponentByClass(UWeaponSystem::StaticClass()));
	float currentAmmo = weaponSystem->WeaponData.currentNbAmmo;

		//if we have more unloaded ammo than what we allow 
	if (weaponSystem->WeaponData.nbAmmoUnload >= WeaponData.nbAmmoLoad)
	{
		//add to our current
		weaponSystem->WeaponData.currentNbAmmo = weaponSystem->WeaponData.nbAmmoLoad;

		//decrease our ammo
		weaponSystem->WeaponData.nbAmmoUnload = weaponSystem->WeaponData.nbAmmoUnload - weaponSystem->WeaponData.nbAmmoLoad;
	}
	else
	{
		weaponSystem->WeaponData.currentNbAmmo = weaponSystem->WeaponData.currentNbAmmo + weaponSystem->WeaponData.nbAmmoUnload;
		weaponSystem->WeaponData.nbAmmoUnload = 0;
	}

	GetOwner()->SetActorHiddenInGame(false);
}

void UTP_WeaponComponent::ManualReloadAmmo()
{
	IsReloading = false;
	//AmmoCalculation();
	UWeaponSystem* weaponSystem = Cast<UWeaponSystem>(Character->GetComponentByClass(UWeaponSystem::StaticClass()));
	float currentAmmo = weaponSystem->WeaponData.currentNbAmmo;


	if (weaponSystem->WeaponData.currentNbAmmo == weaponSystem->WeaponData.nbAmmoLoad)
	{
		return;
	}
	
	if (weaponSystem->WeaponData.currentNbAmmo == 0 && weaponSystem->WeaponData.nbAmmoUnload > WeaponData.nbAmmoLoad)
	{
		return;
	}


	if (weaponSystem->WeaponData.currentNbAmmo != 0 && weaponSystem->WeaponData.nbAmmoUnload > WeaponData.nbAmmoLoad)
	{
		weaponSystem->WeaponData.currentNbAmmo = weaponSystem->WeaponData.nbAmmoLoad;
		
		weaponSystem->WeaponData.nbAmmoUnload = weaponSystem->WeaponData.nbAmmoUnload - (weaponSystem->WeaponData.currentNbAmmo - currentAmmo);
	}
	else if (weaponSystem->WeaponData.nbAmmoUnload <= weaponSystem->WeaponData.nbAmmoLoad)
	{
		auto resultHigherThanAllowed = weaponSystem->WeaponData.currentNbAmmo + weaponSystem->WeaponData.nbAmmoUnload;
		if (resultHigherThanAllowed >= weaponSystem->WeaponData.nbAmmoLoad)
		{
			weaponSystem->WeaponData.currentNbAmmo = weaponSystem->WeaponData.nbAmmoLoad;

			weaponSystem->WeaponData.nbAmmoUnload = weaponSystem->WeaponData.nbAmmoUnload - (weaponSystem->WeaponData.nbAmmoLoad - currentAmmo);
		}
		else
		{
			weaponSystem->WeaponData.currentNbAmmo = weaponSystem->WeaponData.currentNbAmmo + weaponSystem->WeaponData.nbAmmoUnload;
			weaponSystem->WeaponData.nbAmmoUnload = weaponSystem->WeaponData.nbAmmoUnload - currentAmmo;
			if (weaponSystem->WeaponData.nbAmmoUnload <= 0)
			{
				weaponSystem->WeaponData.nbAmmoUnload = 0;
			}
		}
	}


	GetOwner()->SetActorHiddenInGame(false);
}

void UTP_WeaponComponent::AmmoCalculation()
{
	UWeaponSystem* weaponSystem = Cast<UWeaponSystem>(Character->GetComponentByClass(UWeaponSystem::StaticClass()));
	//WeaponData = weaponSystem->WeaponData;

	float currentAmmo = weaponSystem->WeaponData.currentNbAmmo;

		
	//1. current ammo is now equal to the amount that can be allowed by nbAmmoLoad 
		// IF we have enough -> if what we have in unloaded is greater than we allowed
		// current ammo will equal = nbAmmoLoad, and we'll decrease our unloaded ammo by the same amount added to current


	//if we have more unloaded ammo than what we allow 
	if (weaponSystem->WeaponData.nbAmmoUnload > WeaponData.nbAmmoLoad)
	{
		//add to our current
		weaponSystem->WeaponData.currentNbAmmo = weaponSystem->WeaponData.nbAmmoLoad;
		
		//decrease our ammo
		weaponSystem->WeaponData.nbAmmoUnload = weaponSystem->WeaponData.nbAmmoUnload - weaponSystem->WeaponData.nbAmmoLoad;
	}
	//if lower
	if (weaponSystem->WeaponData.nbAmmoUnload < WeaponData.nbAmmoLoad)
	{
		//add to our current what's leftover in reserve
		weaponSystem->WeaponData.currentNbAmmo = weaponSystem->WeaponData.currentNbAmmo + weaponSystem->WeaponData.nbAmmoUnload;
		weaponSystem->WeaponData.nbAmmoUnload = 0;
	}



}

void UTP_WeaponComponent::BindWeaponToCharacter()
{
	AWeapon* weaponActor = Cast<AWeapon>(GetOwner());
	UWeaponSystem* weaponSystem = Cast<UWeaponSystem>(Character->GetComponentByClass(UWeaponSystem::StaticClass()));


	if (IsValid(weaponActor) && IsValid(weaponSystem))
	{
		// Attach the weapon to the First Person Character
		FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, true);
		weaponActor->AttachToComponent(Character->GetMesh1P(), AttachmentRules, FName(TEXT("GripPoint")));
		weaponActor->IsHeld = true;

		// Register so that Fire is called every time the character tries to use the item being held
		Character->OnUseItem.AddDynamic(this, &UTP_WeaponComponent::Fire);
		Character->OnReloadGun.AddDynamic(this, &UTP_WeaponComponent::ManualReloadAmmo);
	
		auto getWeaponData = Cast<UTP_WeaponComponent>(weaponActor->GetComponentByClass(UTP_WeaponComponent::StaticClass()));
		weaponSystem->WeaponData = getWeaponData->WeaponData;//TODO
		weaponActor->SetActorHiddenInGame(false);
	}
}

void UTP_WeaponComponent::UnbindWeaponToCharacter()
{
	AWeapon* weaponActor = Cast<AWeapon>(GetOwner());

	if (IsValid(weaponActor))
	{
		weaponActor->IsHeld = false;

		// Register so that Fire is called every time the character tries to use the item being held
		Character->OnUseItem.RemoveDynamic(this, &UTP_WeaponComponent::Fire);
		Character->OnReloadGun.RemoveDynamic(this, &UTP_WeaponComponent::ManualReloadAmmo);

		weaponActor->SetActorHiddenInGame(true);
		weaponActor->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
	}
}

void UTP_WeaponComponent::AttachWeapon(AFPSgameCharacter* TargetCharacter)
{
	Character = TargetCharacter;
	
	if(IsValid(Character))
	{
		AWeapon* weaponActor = Cast<AWeapon>(GetOwner());
		UWeaponSystem* weaponSystem = Cast<UWeaponSystem>(Character->GetComponentByClass(UWeaponSystem::StaticClass()));
	
		//add first and second weapon  
		if (weaponSystem->Weapons.Num() <= 2)
		{
			if (weaponActor != nullptr)
			{
				weaponSystem->Weapons.AddUnique(weaponActor);
			}
			
			if (weaponSystem->Weapons.Num() == 1)	
			{
				BindWeaponToCharacter();
			}
			else
			{
				GetOwner()->SetActorHiddenInGame(true);
			}
		}

	}
}