// Copyright Epic Games, Inc. All Rights Reserved.

#include "FPSgame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, FPSgame, "FPSgame" );
 