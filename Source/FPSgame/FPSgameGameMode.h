// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FPSgameGameMode.generated.h"

UCLASS(minimalapi)
class AFPSgameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFPSgameGameMode();
};



