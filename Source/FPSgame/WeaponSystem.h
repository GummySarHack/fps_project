// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Weapon.h"
#include "WeaponData.h"
#include "WeaponSystem.generated.h"





UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FPSGAME_API UWeaponSystem : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UWeaponSystem();

protected:

public:

	FWeaponData WeaponData;
	
	UPROPERTY()
	AFPSgameCharacter* Character = nullptr;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	FWeaponData GetWeaponData() { return WeaponData; };

	UFUNCTION(BlueprintCallable, BlueprintPure)
	TArray<class ABomb*> GetBombs() { return Bombs; };

	UFUNCTION()
	void SwitchWeapon();

	UPROPERTY()
	TArray<AWeapon*> Weapons;

	UPROPERTY()
	TArray<class ABomb*> Bombs;
};
