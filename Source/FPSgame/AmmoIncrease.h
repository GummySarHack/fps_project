// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FPSgameCharacter.h"
#include "Components/SphereComponent.h"

#include "AmmoIncrease.generated.h"

UCLASS()
class FPSGAME_API AAmmoIncrease : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAmmoIncrease();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
	USphereComponent* SphereComp;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoIncrease")
	float NbAmmoIncrease;

	UFUNCTION()
	void OnSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, 
							  AActor* OtherActor, 
							  UPrimitiveComponent* OtherComp, 
							  int32 OtherBodyIndex, 
							  bool bFromSweep, 
							  const FHitResult& SweepResult);

	AFPSgameCharacter* Character;
};
