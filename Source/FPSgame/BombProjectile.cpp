// Fill out your copyright notice in the Description page of Project Settings.


#include "BombProjectile.h"

#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "Enemy.h"

ABombProjectile::ABombProjectile()
{
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;

	// Die after 3 seconds by default
	InitialLifeSpan = 120.0f;
}

void ABombProjectile::DoOnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	/*
	*	This is a test with a weapon type actor
	*/
	auto enemy = Cast<AEnemy>(OtherActor);

	if (IsValid(enemy))
	{
		if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Hit Weapon!"));

		//attach projectile to weapon
		FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, true);
		this->AttachToComponent(enemy->GetMesh(), AttachmentRules, FName(TEXT("None")));

		ProjectileMovement->StopMovementImmediately();

		//kills an enemy character after 3s
		FTimerDelegate timerDel;
		timerDel.BindLambda([this, enemy]
		{
			DestroyActor(enemy);
		});
		
		this->GetWorldTimerManager().SetTimer(UnusedHandle, timerDel, 3.f, false);
	}
	else
	{
		if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr) && OtherComp->IsSimulatingPhysics())
		{
			OtherComp->AddImpulseAtLocation(GetVelocity() * 100.0f, GetActorLocation());
		}
	}
}

void ABombProjectile::DestroyActor(AEnemy *enemy)
{
	if (GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Destroy Weapon!"));


	enemy->Destroy();
	Destroy();
}