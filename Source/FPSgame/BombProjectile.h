// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FPSgameProjectile.h"
#include "BombProjectile.generated.h"

/**
 * 
 */
UCLASS()
class FPSGAME_API ABombProjectile : public AFPSgameProjectile
{
	GENERATED_BODY()

	FTimerHandle UnusedHandle;

	ABombProjectile();
	
	
	void DoOnHit(UPrimitiveComponent* HitComp,
			   AActor* OtherActor, 
			   UPrimitiveComponent* OtherComp, 
			   FVector NormalImpulse, 
			   const FHitResult& Hit) override;

	void DestroyActor(class AEnemy* enemy);

};
