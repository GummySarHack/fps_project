# Project status
In progress



## Test Unreal Engine C++ -- Weapon management system.

From the Unreal Engine 5.1 First-Person Shooter template, add a weapon system that manages and stores weapons. The system must also handle ammunition.
The default template does not handle more than one weapon. The new system needs to be able to handle multiple weapons.
Each weapon must have their own ammunition count. Each weapon has both a loaded ammunition amount and a total amount of ammunition.
When a weapon fires, the amount of loaded ammunition decreases. When there is no more loaded ammunition, the weapon must be reloaded. While reloading, the weapon hides itself from the player for a scripted amount of time.
All logic must be in C++. Blueprint must be only used for visual/asset management purposes. All collisions must be handled by C++ while collision profile can be done in Blueprints.


## First Stage
* Create a new structure that holds the weapon data:
* Number of ammunitions loaded.
    * Total ammunition not loaded.
    * Weapon assets.
    * Reload time.
* Create a new component (not a scene component) that handles the weapon system.
* Add new weapon assets:
    * For simplicity, copy the existing weapon and change its texture color.
* Add new pickable weapon assets:
    * Each weapon must have corresponding ammunition.
* The weapon must check the weapon structure ammunition amount before firing.
* When firing the number of loaded ammunitions decreases
* When empty, the weapon reloaded automatically:
    * The weapon and the arms become hidden from the player.



## Second Stage
* Add inventory
* Player can hold two weapons
* UI that shows inventory
* Player can select with input keys which weapon to witch


## Third Stage
* Player can shoot x amount of grenades
* New Enemy actor
* New projectile actor 
* If Enemy type Actor is hit, grenade sticks on it 
	*Ater 3 seconds, Enemy actor is destroyed

## Fourth Stage
* Add Damage taken for gun projectiles
* Add ragdoll logic
* Add Niagara effects on death


